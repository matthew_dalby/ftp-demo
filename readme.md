#FTP Demo example#

This project will produce an executable jar that will allow us to reproduce the issues related to the Foundation Sftp class. Please note the references to the foundation project are JDK 8 specific, and will require that foundation is built with JDK 8. 

Notes:
1. On VM instances, if JDK 1.8 is installed, it is not enabled by default. Instead you will need fully qualified path to jdk 1.8

/usr/lib/jvm/java-1.8.0

2. The project includes the build artifacts, so the compilation step is not necessary, which has the jdk 1.8 oundation build step as a precursor.

##Building:##
gradle build shadowJar

##Executing:##
java -jar build/libs/ftp-demo-all.jar [remote host] [remote account] [password] [remote file] [local dest directory]

##Example:##
Note: I have created an account on my development VM that we can use to reproduce.
java -jar build/libs/ftp-demo-all.jar dev-server-all-apps-matthewd01 leadsimport leadsimport /tmp/sample.csv /tmp