import java.io.*;
import progreso.integration.Sftp;

public class FtpExample{


	public static void main(String[] args){

		System.out.println("FtpExample");

		if(args.length!=5){
			System.out.println("Usage:");
			System.out.println("	java -jar ftp-example.jar [remote host] [remote account] [password] [remote file] [local directory]");		
			return;
		}

		try{

			String remoteHost, remoteAccount, password, remoteFile, localDirectory;
			remoteHost = args[0];
			remoteAccount = args[1];
			password = args[2];
			remoteFile = args[3];
			localDirectory = args[4];

			Sftp ftp = new Sftp(remoteAccount, password, remoteHost);
			ftp.get(remoteFile, localDirectory);

			System.out.println("Operation successful!");
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("Operation failed");
		}
	}


}